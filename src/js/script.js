
/*
var myModule = angular.module("myModule", []);
*/


/*
The scope is a JavaScript object with properties and methods,
 which are available for both the view and the controller.
 https://www.w3schools.com/angular/angular_scopes.asp
 */

/*
myModule.controller("myController",function($scope) {


  var Employee={
    firstName:"David",
    lastName:"Roger",
    gender:"male"
  };

  $scope.employe =Employee;


});*/

var myModule = angular
  .module("myModule", [])
  .controller("myController",function($scope) {


    var Employee={
      firstName:"David",
      lastName:"Roger",
      gender:"male"
    };

    $scope.employe =Employee;


  });



